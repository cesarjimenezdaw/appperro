package com.example.pruebas.modelo


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers


interface PerroAPI {
    @Headers("Accept: application/json")
    @GET("breeds/image/random")
    fun getUrl(): Call<Perro>
}