package com.example.pruebas

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import coil.ImageLoader
import coil.compose.AsyncImage
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import com.example.apis.ui.theme.APISTheme
import com.example.pruebas.modelo.PerroAPI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import coil.compose.rememberImagePainter


class MainActivity : ComponentActivity() {

    private lateinit var retrofit : Retrofit
    private var texto: String = "Pruebas"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)

        setContent {
            APISTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(texto)

                    Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
                        val imageUrl = remember{ mutableStateOf("") }
                        Button(onClick = {imageUrl.value = obtenerDatos(retrofit)}) {      }

                        //loadImage2(texto)


                       // Image(painter = painterResource(id = imageUrl.value), contentDescription = )
                        Text(text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                                "Sed ut velit eu libero consequat faucibus. Quisque convallis sapi" +
                                "en a est fermentum, at interdum purus cursus. Fusce tincidunt quam n" +
                                "ec mauris ultrices, quis varius sem tempus. Nam tincidunt nunc eget sapien" +
                                " tempus, ut accumsan dolor bibendum. Pellentesque habitant morbi tristique senec" +
                                "tus et netus et malesuada fames ac turpis egestas. Suspendisse id lacinia tortor. " +
                                "Ut pulvinar libero eget eros fermentum, a sagittis nisi sagittis. Phasellus quis mi vel" +
                                " nisi lacinia hendrerit. Nullam interdum, lorem vel finibus condimentum, dolor orci con" +
                                "vallis neque, ac efficitur nisi sapien ac nulla. Vivamus id eros sit amet urna con" +
                                "sectetur pellentesque. Phasellus ut ligula eleifend, interdum lectus sed, c" +
                                "ondimentum mi. Mauris volutpat ligula a lorem interdum, a feugiat eros conva" +
                                "llis. Fusce eleifend sem vel accumsan sollicitudin.")
                    }
                }
            }
        }
    }

    private fun obtenerDatos(retrofit: Retrofit):String{
        var texto = "";
        CoroutineScope(Dispatchers.IO).launch {
            val call = retrofit.create(PerroAPI::class.java).getUrl().execute()
            val perros = call.body()
            if(call.isSuccessful){
                texto = perros?.getUrl().toString()
            }else{
                texto = "Ha habido un error"
            }
        }
        Thread.sleep(5000)
        return  texto
    }
}
@Composable
fun loadImage2 (url: String ){
    AsyncImage(model = url, contentDescription = null )
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "url perro $name!",
        modifier = modifier
    )
}