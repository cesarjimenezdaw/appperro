package com.example.pruebas

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.pruebas.ui.theme.PruebasTheme


//Aqui va el codigo "real"
class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PruebasTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    //Greeting("Android")
                    //numeros(20,5)
                    //PruebaBotones()
                    Macetin()
                }
            }
        }
    }
}
//comentario
fun cosa() : String{
    return "Cosa"
}
//USO TEXTOS CON ESTILOS
/* Compose es cuando renderiza y se vuelve a llamar cuando cambia la interfaz
* */
@Composable
fun Greeting2(name: String, modifier: Modifier = Modifier) {
    var cosa = cosa();
    Text(
        text = "Hello $name! $cosa",
        color = Color.Red,
        fontSize = 20.sp,
        modifier = Modifier.padding(10.dp)
    )
    Text(
        text = stringResource(id = R.string.saludo),
        color = Color.Red,
        fontSize = 20.sp,
        modifier = Modifier.padding(10.dp)
    )
    //bombilla y extract para guardarlos en res/values/strings.xml
    Text(text = stringResource(R.string.hola2))
    Text(text = "patata")
}
//USO FOR
@Composable
fun numeros(cantidadNumeros : Int, numerosPorfila : Int){
    var col : Color
    var numeroActual =1;
    var filas = if (cantidadNumeros%numerosPorfila==0) cantidadNumeros/numerosPorfila else cantidadNumeros/numerosPorfila +1
    //var gato = painterResource(id = R.drawable.ic_launcher_foreground)
    var gato = painterResource(id = R.drawable.gatopaloma)

    Image(
        painter = gato,
        contentDescription = stringResource(R.string.un_gato_sin_pelo),
        contentScale = ContentScale.Crop,
        alpha = 0.9f
    )
    Column(
        modifier = Modifier.padding(10.dp)
    ){

        for (i in 1..filas){
            Row() {
                for (i in 1..numerosPorfila){
                    if(numeroActual<=cantidadNumeros){
                        if (numeroActual%2==0){
                            col = Color.Red;
                        }else{
                            col = Color.Blue;
                        }
                        Text(
                            text = numeroActual.toString(),
                            color = col
                        )
                        numeroActual ++;
                    }
                }
            }
        }
    }
}
//USO BOTONES
@Composable
fun PruebaBotones(){
    //sin esto no se actualiza el valor al onClick
    //num = remember esto actualiza la intefaz
    var num by remember {
        mutableStateOf(1)
    }
    Column() {
        Button(
            onClick = {
                num = (1..10).random()
            }
        )
        {
            Text ("Boton")
        }
        Text (num.toString())
    }
}

/**Ejercicio de Botones/imagenes
 * Al pulsar el boton el estado de la planta variará, en el primer loop empieza con la maceta varia
 * una vez recolectada la flor volverá a una de las fases intermedias (no a maceta vacia)
 * Hay un contador de recoleccion que dependerá de la cantidad de veces que se complete el ciclo y
 * el numero de valor que se genere aleatoriamente
 * Si se escriben las cosas asi en verdecito parece como hasta mas serio e importante
 * */
//view tool resource manager + import drawble
//view tooll commit
@Composable
fun Macetin (){
    //La fase en la que esta la maceta
    var estado by remember{
        mutableStateOf(1)
    }
    //El valor del contador de recoleccion
    var contador by remember {
        mutableStateOf(2)
    }
    Column() {

        var macetin = painterResource(id = R.drawable.florecita_1)
        Image(
            painter = macetin,
            contentDescription = "maceta",
            contentScale = ContentScale.Crop,
            alpha = 0.9f
        )
        Button(
            onClick = {
                estado += 1;
            }
        )
        {
            Text (stringResource(R.string.crecer))
        }
        Text (contador.toString())

    }
}
///////////////////////////////////PREVIEW//////////////////////////////////////////////////////
//Esto para "falsear" como quedaría
@Preview(showBackground = true)
@Composable
fun Preview() {
    PruebasTheme {
        //Greeting("Android")
        //numeros(21,5)
        PruebaBotones()
        //Macetin()
    }
}
///////////////////////////////////PREVIEW//////////////////////////////////////////////////////


